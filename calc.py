def sumar(x,y):
    return x+y

def restar(x,y):
    return x-y

print('El resultado de sumar 1 y 2 es:', sumar(1,2))
print('El resultado de sumar 3 y 4 es:', sumar(3,4))
print('El resultado de restar 5 de 6 es:', restar(6,5))
print('El resultado de restar 7 de 8 es:', restar(8,7))
